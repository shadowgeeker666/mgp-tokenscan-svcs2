package com.mgpchain.tokenscan.job.domain

data class ChainInfo(
    val block_cpu_limit: Int,
    val block_net_limit: Int,
    val chain_id: String,
    val fork_db_head_block_id: String,
    val fork_db_head_block_num: Long,
    val head_block_id: String,
    val head_block_num: Long,
    val head_block_producer: String,
    val head_block_time: String,
    val last_irreversible_block_id: String,
    val last_irreversible_block_num: Long,
    val server_version: String,
    val server_version_string: String,
    val virtual_block_cpu_limit: Int,
    val virtual_block_net_limit: Int
)