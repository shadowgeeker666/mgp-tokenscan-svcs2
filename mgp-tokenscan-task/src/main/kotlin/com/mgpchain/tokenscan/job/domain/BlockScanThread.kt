package com.mgpchain.tokenscan.job.domain

import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONArray
import com.alibaba.fastjson.JSONObject
import com.mgpchain.blockscan.common.until.CallableTemplate
import com.mgpchain.blockscan.entity.domain.MbActions
import com.mgpchain.blockscan.entity.domain.MbBlock
import com.mgpchain.blockscan.entity.domain.MbTransactionDetail
import com.mgpchain.tokenscan.job.BlockScanHandler
import com.mgpchain.tokenscan.job.common.MgpChainApi
import org.apache.commons.beanutils.BeanUtilsBean2
import org.slf4j.LoggerFactory

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/7/15
 * @description：
 * @modified By：
 **/
class BlockScanThread : CallableTemplate<List<MbTransactionDetail>>() {
    var blockNum: String? = null
    fun blockScanThread(blockNum: String) {
        this.blockNum = blockNum
    }

    override fun process(): List<MbTransactionDetail>? {
        return queryBlockOnChain(blockNum!!)
    }

    private fun queryBlockOnChain(blockNumOrId: String): List<MbTransactionDetail>? {
        val response = MgpChainApi.getBlock(blockNumOrId = blockNumOrId)
        val mbBlock = MbBlock()
        BeanUtilsBean2.getInstance().copyProperties(mbBlock, response)
        mbBlock.blockNum = response.block_num
        log.info("查询底层区块高度：${mbBlock.blockNum}")
        val result = arrayListOf<MbTransactionDetail>()
        //如果transactions不为空，则同步transaction
        response.transactions?.forEach {
            if (it["trx"] !is JSONObject) return@forEach
            val trx = it["trx"] as JSONObject
            val transactionId = trx["id"].toString()

            val transaction = trx["transaction"] as JSONObject
            val actions = (transaction["actions"] as JSONArray)
            val actionList = mutableListOf<MbActions>()
            actions?.forEach { action ->
                val actionJson = action as JSONObject
                val mbAction = MbActions()
                mbAction.account = actionJson["account"].toString()
                mbAction.name = actionJson["name"].toString()
                mbAction.blockNum = mbBlock.blockNum
                mbAction.actAt = mbBlock.timestamp
                mbAction.transactionId = transactionId
                mbAction.trace = actionJson.toString()
                actionList.add(mbAction)
            }

            actionList?.filter { mbActions -> mbActions.name == "transfer" }?.forEach { mbAction ->
                val data = JSON.parseObject(mbAction.trace)["data"] as JSONObject
                val from = data["from"].toString()
                val to = data["to"].toString()
                val quantity = data["quantity"]
                val memo = data["memo"]
                log.info("transactionId:${transactionId}---from:${from}----to:${to}----quantity:${quantity}")
                val mbTransactionDetail = MbTransactionDetail()
                mbTransactionDetail.transactionId = transactionId
                mbTransactionDetail.txTime = mbAction.actAt
                mbTransactionDetail.blockNum = mbAction.blockNum
                mbTransactionDetail.account = mbAction.account
                mbTransactionDetail.fromAccount = from
                mbTransactionDetail.toAccount = to
                mbTransactionDetail.quantity = quantity.toString()
                mbTransactionDetail.memo = memo.toString()
                mbTransactionDetail.txName = mbAction.name
                mbTransactionDetail.amount = quantity.toString().split(" ")[0].toBigDecimal()
                mbTransactionDetail.symbol = quantity.toString().split(" ")[1]

                result.add(mbTransactionDetail)
            }
        }
        return if (result.size > 0) result else null
    }

    private val log = LoggerFactory.getLogger(BlockScanHandler::class.java)
}