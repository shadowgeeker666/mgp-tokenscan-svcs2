package com.mgpchain.tokenscan.job.domain

import com.alibaba.fastjson.JSONObject
import java.util.*

data class BlockInfo(
    val action_mroot: String,
    val block_extensions: List<String>?,
    val block_num: Long,
    val confirmed: Int,
    val header_extensions: List<String>?,
    val id: String,
    val new_producers: Any?,
    val previous: String,
    val producer: String,
    val producer_signature: String,
    val ref_block_prefix: Long,
    val schedule_version: Int,
    val timestamp: Date,
    val transaction_mroot: String,
    val transactions: List<JSONObject>?
)