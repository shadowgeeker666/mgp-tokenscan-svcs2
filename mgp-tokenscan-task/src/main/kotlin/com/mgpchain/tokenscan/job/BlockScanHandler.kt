package com.mgpchain.tokenscan.job

import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONArray
import com.alibaba.fastjson.JSONObject
import com.mgpchain.blockscan.common.biz.service.*
import com.mgpchain.blockscan.common.until.CallableTaskFrameWork
import com.mgpchain.blockscan.common.until.CallableTemplate
import com.mgpchain.blockscan.entity.domain.*
import com.mgpchain.tokenscan.job.common.MgpChainApi
import com.mgpchain.tokenscan.job.domain.BlockScanThread
import com.xxl.job.core.context.XxlJobHelper
import com.xxl.job.core.handler.annotation.XxlJob
import org.apache.commons.beanutils.BeanUtilsBean2
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class BlockScanHandler {
    @XxlJob("blockTrxScanHandler")
    @Transactional
    open fun blockScanHandler() {
        //查询数据库的高度
        val latestBlockInDb = mbSysConfigService.getByName("MAX_SYNC_HEIGHT")
        var height = latestBlockInDb.cfgValue.toLong()
        log.info("扫描数据库最新同步高度为：${height}")
        //查询链的最高高度
        val headBlockNum = queryLatestBlockOnChain()
        log.info("查询链的最高高度：${headBlockNum}")

        XxlJobHelper.log("latest block in db: $height  || latest block on chain: $headBlockNum")


        val scanHeight = if ((height + 100) < headBlockNum) height + 100 else headBlockNum

        val tasks = ArrayList<CallableTemplate<List<MbTransactionDetail>>>()
        //同步区块
        /*if (scanHeight > height) {
            ((height + 1)..scanHeight).toList().forEach {
                val callable = BlockScanThread()
                callable.blockScanThread("$it")
                tasks.add(callable)
            }
            val dbResult = mutableListOf<MbTransactionDetail>()
            callableTaskFrameWork.submitsAll(tasks).filterNotNull().forEach { dbResult += it }

            if (!dbResult.isNullOrEmpty()) {
                mbTransactionDetailService.savaAll(dbResult)
            }
        }*/


        if (scanHeight > height) {
            ((height + 1)..scanHeight).toList().forEach {
                queryBlockOnChain("$it")
            }
        }
        log.info("保存MAX_SYNC_HEIGHT：${scanHeight}")
        latestBlockInDb.cfgValue = scanHeight.toString()
        mbSysConfigService.save(latestBlockInDb)

        return
    }

    private fun queryBlockOnChain(blockNumOrId: String) {
        val response = MgpChainApi.getBlock(blockNumOrId = blockNumOrId)
        val mbBlock = MbBlock()
        BeanUtilsBean2.getInstance().copyProperties(mbBlock, response)
        mbBlock.blockNum = response.block_num
        log.info("查询底层区块高度：${mbBlock.blockNum}")
        //如果transactions不为空，则同步transaction
        response.transactions?.forEach {
            if (it["trx"] !is JSONObject) return@forEach
            val trx = it["trx"] as JSONObject
            val transactionId = trx["id"].toString()

            val transaction = trx["transaction"] as JSONObject
            val actions = (transaction["actions"] as JSONArray)
            val actionList = mutableListOf<MbActions>()
            actions?.forEach { action ->
                val actionJson = action as JSONObject
                val mbAction = MbActions()
                mbAction.account = actionJson["account"].toString()
                mbAction.name = actionJson["name"].toString()
                mbAction.blockNum = mbBlock.blockNum
                mbAction.actAt = mbBlock.timestamp
                mbAction.transactionId = transactionId
                mbAction.trace = actionJson.toString()
                actionList.add(mbAction)
            }
            val result = arrayListOf<MbTransactionDetail>()
            actionList?.filter { mbActions -> mbActions.name == "transfer" }?.forEach { mbAction ->
                val data = JSON.parseObject(mbAction.trace)["data"] as JSONObject
                val from = data["from"].toString()
                val to = data["to"].toString()
                val quantity = data["quantity"]
                val memo = data["memo"]
                log.info("transactionId:${transactionId}---from:${from}----to:${to}----quantity:${quantity}")
                val mbTransactionDetail = MbTransactionDetail()
                mbTransactionDetail.transactionId = transactionId
                mbTransactionDetail.txTime = mbAction.actAt
                mbTransactionDetail.blockNum = mbAction.blockNum
                mbTransactionDetail.account = mbAction.account
                mbTransactionDetail.fromAccount = from
                mbTransactionDetail.toAccount = to
                mbTransactionDetail.quantity = quantity.toString()
                mbTransactionDetail.memo = memo.toString()
                mbTransactionDetail.txName = mbAction.name
                mbTransactionDetail.amount = quantity.toString().split(" ")[0].toBigDecimal()
                mbTransactionDetail.symbol = quantity.toString().split(" ")[1]

                result.add(mbTransactionDetail)
            }
            if (result.size > 0) {
                mbTransactionDetailService.savaAll(result)
            }
        }
    }

    private fun queryLatestBlockOnChain(): Long {
        val response = MgpChainApi.getInfo()
        return response.head_block_num
    }

    private val log = LoggerFactory.getLogger(BlockScanHandler::class.java)

    @Autowired
    lateinit var callableTaskFrameWork: CallableTaskFrameWork

    @Autowired
    lateinit var mbSysConfigService: MbSysConfigService

    @Autowired
    lateinit var mbTransactionDetailService: MbTransactionDetailService
}

