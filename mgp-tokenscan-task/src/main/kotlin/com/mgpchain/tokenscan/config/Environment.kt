package com.mgpchain.tokenscan.config

import com.mgpchain.blockscan.common.biz.env.BaseEnv


object Environment {

    @JvmField
    var MYSQL_URL = BaseEnv.env("MYSQL_URL", "jdbc:mysql://jp-api.zw.mgps.me:13306/mgp-blockscan?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&useSSL=false")

    @JvmField
    var MYSQL_USERNAME = BaseEnv.env("MYSQL_USERNAME", "chenhang-rw")

    @JvmField
    var MYSQL_PASSWORD = BaseEnv.env("MYSQL_PASSWORD", "chang@123456")

    @JvmField
    var MYSQL_DRIVER = BaseEnv.env("MYSQL_DRIVER", "com.mysql.jdbc.Driver")


    @JvmField
    var TASK_EXECUTOR_IP = BaseEnv.env("TASK_EXECUTOR_IP", "172.27.142.45")

    @JvmField
    var TASK_EXECUTOR_PORT = BaseEnv.env("TASK_EXECUTOR_PORT", 5000)

    @JvmField
    var TASK_JOB_ADMIN_URL = BaseEnv.env("TASK_JOB_ADMIN_URL", "http://172.17.0.1:9000")

    @JvmField
    var TASK_APP_NAME = BaseEnv.env("TASK_APP_NAME", "mgp-blockscan-task-executor")

    @JvmField
    var MGP_NODE = BaseEnv.env("MGP_NODE", "http://sh-expnode.vm.mgps.me:8888")

}

