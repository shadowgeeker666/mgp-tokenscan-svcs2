package com.mgpchain.tokenscan.config

import com.mgpchain.blockscan.common.biz.env.BaseEnv
import com.mgpchain.blockscan.common.biz.service.SysConfigService
import org.reflections.Reflections
import org.reflections.scanners.SubTypesScanner
import org.reflections.util.FilterBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.stereotype.Component


@Component
class SysConfigLoadManager {
    var logger = LoggerFactory.getLogger(javaClass)

    @Autowired lateinit var context: ConfigurableApplicationContext
    @Autowired lateinit var sysConfigService: SysConfigService

    fun init() {
        val envClz = Reflections("com.mgpchain", SubTypesScanner(false), FilterBuilder().include(".*\\.Environment.class"))

        val envClz1 = envClz.getSubTypesOf(Object::class.java)
        envClz1.forEach { handle(it) }

        val envClz2 = envClz.getSubTypesOf(BaseEnv::class.java)
        envClz2.forEach { handle(it) }
    }

    private fun handle(clz: Class<out Any>) {
        clz.declaredFields.forEach {
            try {
                val value = sysConfigService.findByCfgName(it.name)
                val type = it.type.toString()//得到此属性的类型
                var obj: Any? = null
                if (type.endsWith("String")) {
                    obj = value
                } else if (type.endsWith("int") || type.endsWith("Integer")) {
                    obj = value.toInt()
                } else if (type.endsWith("boolean")) {
                    obj = value.toBoolean()
                } else if (type.endsWith("long")) {
                    obj = value.toLong()
                }

                it.set(clz, obj)
                logger.info("${it.name}:$obj")
            } catch (e: Exception) {
                logger.warn("-------------------------------SysConfigLoadManager load end:${e.message}")
                return@forEach
            } catch (e: NoSuchFieldException) {
                logger.warn("-------------------------------SysConfigLoadManager loan exception : ${e.message}")
                return@forEach
            } catch (e: Exception) {
                logger.error("-------------------------------SysConfigLoadManager load end:${e.message}", e)
                System.exit(SpringApplication.exit(context))
            }
        }
    }
}

