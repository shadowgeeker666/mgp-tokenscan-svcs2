package com.mgpchain.tokenscan.config

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration


@Configuration
@ComponentScan("com.mgpchain")
open class TaskExecutorConfig {
    private val logger = LoggerFactory.getLogger(TaskExecutorConfig::class.java)

    open fun init(): XxlJobSpringExecutor {
        logger.trace("------------ xxlJobExecutor -----------")

        val xxlJobSpringExecutor = XxlJobSpringExecutor()
        xxlJobSpringExecutor.setIp(Environment.TASK_EXECUTOR_IP)
        xxlJobSpringExecutor.setPort(Environment.TASK_EXECUTOR_PORT)
        xxlJobSpringExecutor.setAppname(Environment.TASK_APP_NAME)
        xxlJobSpringExecutor.setAdminAddresses(Environment.TASK_JOB_ADMIN_URL)
        xxlJobSpringExecutor.setLogPath("/tmp/mgp/logs")
        return xxlJobSpringExecutor
    }

    @Bean(initMethod = "start", destroyMethod = "destroy")
    open fun xxlJobSpringExecutor(): XxlJobSpringExecutor {
        return init()
    }
}