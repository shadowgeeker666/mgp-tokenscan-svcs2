//package com.mgpchain.blockscan.config
//
//import com.alibaba.druid.pool.DruidDataSource
//import com.alibaba.druid.support.http.StatViewServlet
//import com.alibaba.druid.support.http.WebStatFilter
//import org.apache.ibatis.session.SqlSessionFactory
//import org.apache.shardingsphere.api.config.sharding.KeyGeneratorConfiguration
//import org.apache.shardingsphere.api.config.sharding.ShardingRuleConfiguration
//import org.apache.shardingsphere.api.config.sharding.TableRuleConfiguration
//import org.apache.shardingsphere.api.config.sharding.strategy.InlineShardingStrategyConfiguration
//import org.apache.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory
//import org.mybatis.spring.SqlSessionFactoryBean
//import org.mybatis.spring.SqlSessionTemplate
//import org.mybatis.spring.annotation.MapperScan
//import org.slf4j.LoggerFactory
//import org.springframework.beans.factory.annotation.Qualifier
//import org.springframework.boot.web.servlet.FilterRegistrationBean
//import org.springframework.boot.web.servlet.ServletRegistrationBean
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import org.springframework.context.annotation.Primary
//import org.springframework.core.io.support.PathMatchingResourcePatternResolver
//import java.sql.SQLException
//import java.util.*
//import javax.sql.DataSource
//import kotlin.collections.HashMap
//
//
//@Configuration
//@MapperScan(sqlSessionTemplateRef = "shardingSqlSessionTemplate", basePackages = ["com.mgpchain.blockscan.shardingmapper"])
//open class DataSourceShardingCfg {
//    private val logger = LoggerFactory.getLogger(javaClass)
//
//    fun createDataSourceMap(): Map<String, DataSource> {
//        val datasource = DruidDataSource()
//
//        datasource.url = Environment.MYSQL_URL
//        datasource.username = Environment.MYSQL_USERNAME
//        datasource.password = Environment.MYSQL_PASSWORD
//        datasource.driverClassName = Environment.MYSQL_DRIVER
//
//        //configuration
//        datasource.initialSize = 20
//        datasource.minIdle = 10
//        datasource.maxActive = 40
//        datasource.maxWait = 5000
//        datasource.timeBetweenEvictionRunsMillis = 10000
//        datasource.minEvictableIdleTimeMillis = 30000
//        datasource.isTestWhileIdle = true
//        datasource.isTestOnBorrow = false
//        datasource.isTestOnReturn = false
//        datasource.isPoolPreparedStatements = true
//        datasource.maxPoolPreparedStatementPerConnectionSize = 20
//        datasource.setConnectionProperties("druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000")
//        datasource.validationQuery = "select now()"
//
//        try {
//            datasource.setFilters("stat,wall,log4j")
//        } catch (e: SQLException) {
//            logger.error("druid configuration initialization filter", e)
//        }
//
//        var dataSourceMap = HashMap<String, DataSource>()
//        dataSourceMap.put("m1", datasource)
//        return dataSourceMap
//    }
//
//    //键生成策略
//    fun getId(): KeyGeneratorConfiguration {
//        return KeyGeneratorConfiguration("SNOWFLAKE", "id")
//    }
//
//    //对mb_block分表策略
//    fun getMbBlockRuleConfiguration(): TableRuleConfiguration {
//        //分库分表描述 单库 分表30
//        var mbBlockTable = TableRuleConfiguration("mb_block", "m$->{1}.mb_block_$->{0..29}")
//        //分库
//        var dataBase = InlineShardingStrategyConfiguration("id", "m$->{1}");
//        mbBlockTable.databaseShardingStrategyConfig = dataBase
//        //分表 30
//        var table = InlineShardingStrategyConfiguration("id", "mb_block_$->{id % 30}")
//        mbBlockTable.tableShardingStrategyConfig = table
//        mbBlockTable.keyGeneratorConfig = getId()
//        return mbBlockTable
//    }
//
//    @Bean("shardingDataSource")
//    open fun dataSource(): DataSource {
//        var shardingRule = ShardingRuleConfiguration()
//        shardingRule.tableRuleConfigs.add(getMbBlockRuleConfiguration())
//        var pro = Properties()
//        //不用打印sql日志
////        pro.put("sql.show", "true")
//        return ShardingDataSourceFactory.createDataSource(createDataSourceMap(), shardingRule, pro)
//    }
//
//    @Bean("shardingEntityManagerFactorySql")
//    open fun shardingEntityManagerFactorySql(): SqlSessionFactory {
//        val factoryBean = SqlSessionFactoryBean()
//        factoryBean.setDataSource(dataSource())
//        val resolver = PathMatchingResourcePatternResolver()
//        factoryBean.setMapperLocations(resolver.getResources("classpath*:mapper/*.xml"))
//        val configuration = org.apache.ibatis.session.Configuration()
//        configuration.isMapUnderscoreToCamelCase = true
//        factoryBean.setConfiguration(configuration)
//        return factoryBean.getObject()
//    }
//
//    @Bean("shardingSqlSessionTemplate")
//    @Throws(Exception::class)
//    open fun sqlSessionTemplate(@Qualifier("shardingEntityManagerFactorySql") sqlSessionFactory: SqlSessionFactory): SqlSessionTemplate {
//        return SqlSessionTemplate(sqlSessionFactory)
//    }
//
//    @Bean
//    open fun viewServletRegistrationBean(): ServletRegistrationBean {
//        val srb = ServletRegistrationBean(StatViewServlet(), "/druid/*")
//        srb.addInitParameter("loginUsername", "root")
//        srb.addInitParameter("loginPassword", "root")
//        //是否可以重置数据
//        srb.addInitParameter("resetEnable", "true")
//        return srb
//    }
//
//    @Bean
//    open fun filter(): FilterRegistrationBean {
//        val frb = FilterRegistrationBean(WebStatFilter())
//        //设置过滤器过滤路径
//        frb.addUrlPatterns("/*")
//        //忽略过滤的形式
//        frb.addInitParameter(
//            "exclusions",
//            "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*"
//        )
//        return frb
//    }
//}