package com.mgpchain.tokenscan.webapi

import com.mgpchain.tokenscan.job.BlockScanHandler
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/ops")
class OpsController {

    @RequestMapping("/version")
    @ApiOperation("版本", httpMethod = "GET")
    fun version() = "0.0.1"

    @RequestMapping("/test")
    fun test() = blockScanHandler.blockScanHandler()

    @Autowired lateinit var blockScanHandler: BlockScanHandler
}