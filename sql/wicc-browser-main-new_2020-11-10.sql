# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: rm-wz9661496wa07c20l.mysql.rds.aliyuncs.com (MySQL 5.7.25-log)
# Database: wicc-browser-main-new
# Generation Time: 2020-11-10 07:28:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table app_stats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_stats`;

CREATE TABLE `app_stats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stats_date` varchar(32) DEFAULT NULL COMMENT '统计日期',
  `contract_address` varchar(64) DEFAULT NULL COMMENT '应用合约地址',
  `contract_reg_id` varchar(32) DEFAULT NULL COMMENT '应用合约regid',
  `transaction_count` int(11) DEFAULT NULL COMMENT '交易总数',
  `active_address_count` int(11) DEFAULT NULL COMMENT '活跃地址数',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_as_statsdate` (`stats_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table contract
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contract`;

CREATE TABLE `contract` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contract_reg_id` varchar(20) DEFAULT NULL COMMENT '注册编号',
  `address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `contract_name` varchar(128) DEFAULT NULL COMMENT '合约名称',
  `contract_name_en` varchar(128) DEFAULT NULL,
  `contract_type` smallint(6) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '100: Initial; 200:confirmed',
  `type` int(11) DEFAULT '100' COMMENT '类型 [200:token, 300:application, 100:unkown(other)]',
  `wicc_balance` decimal(32,8) DEFAULT '0.00000000',
  `transaction_count` int(11) DEFAULT '0' COMMENT '交易总笔数',
  `transaction_amount` decimal(32,8) DEFAULT '0.00000000' COMMENT '交易总金额（wicc）',
  `transaction_amount_wgrt` decimal(32,8) DEFAULT NULL COMMENT '交易总金额（wgrt）',
  `transaction_amount_wusd` decimal(32,8) DEFAULT NULL COMMENT '交易总金额（wusd）',
  `last_term_transaction_count` int(11) DEFAULT '0' COMMENT '最后一个统计周期交易笔数',
  `last_stats_height` int(11) DEFAULT '0' COMMENT '最新统计高度',
  `program_content` mediumtext COMMENT '程序内容',
  `description_cn` varchar(1024) DEFAULT NULL COMMENT '描述',
  `description_en` varchar(1024) DEFAULT NULL,
  `real_created_at` datetime DEFAULT NULL COMMENT '链上创建时间',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `creator_address` varchar(64) DEFAULT NULL COMMENT '创建者地址',
  `created_tx_height` int(11) DEFAULT NULL COMMENT '合约创建时的区块高度',
  `created_tx_hash` varchar(256) DEFAULT NULL COMMENT '创建合约的txhash',
  `params_template` text,
  `logo_url` varchar(256) DEFAULT NULL COMMENT 'logo',
  `run_url` varchar(1024) DEFAULT NULL COMMENT '运行地址',
  `show_status` int(4) DEFAULT '0' COMMENT '状态 0-上架 1-下架',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_address` (`address`) USING BTREE,
  KEY `idx_con_tc` (`transaction_count`) USING BTREE,
  KEY `idx_last_tran_count` (`last_term_transaction_count`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table contract_url
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contract_url`;

CREATE TABLE `contract_url` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contract_address` varchar(128) DEFAULT NULL COMMENT '合约地址',
  `type` int(11) DEFAULT NULL COMMENT '类型 100-官方网址，200-社交媒体，300-白皮书，400-Gitlab',
  `display_label` varchar(128) DEFAULT NULL COMMENT '显示',
  `medium_logo_url` varchar(1024) DEFAULT NULL COMMENT '社交媒体的图片链接',
  `medium_name` varchar(64) DEFAULT NULL COMMENT '社交媒体名称',
  `url` varchar(1024) DEFAULT NULL COMMENT '链接',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table exp_sys_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_sys_config`;

CREATE TABLE `exp_sys_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(64) DEFAULT NULL COMMENT '模块（openAPi,risk,pay,repay,debt）',
  `name` varchar(128) NOT NULL COMMENT '配置变量名',
  `value` text COMMENT '配置变量值',
  `description` varchar(256) DEFAULT NULL COMMENT '说明',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table miner
# ------------------------------------------------------------

DROP TABLE IF EXISTS `miner`;

CREATE TABLE `miner` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(64) DEFAULT NULL COMMENT '矿工地址',
  `miner_pubkey` varchar(128) DEFAULT NULL COMMENT '矿工pk',
  `miner_name` varchar(128) DEFAULT NULL COMMENT '矿工名字',
  `website_url` varchar(1024) DEFAULT NULL COMMENT '网站地址',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `vote_count` int(11) DEFAULT NULL COMMENT '得票数',
  `produced_block_count` int(11) DEFAULT NULL COMMENT '已产生的区块数量',
  `description` text COMMENT '描述',
  `lost_block_count` int(11) DEFAULT NULL COMMENT '丢失区块数',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table node
# ------------------------------------------------------------

DROP TABLE IF EXISTS `node`;

CREATE TABLE `node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `uuid` varchar(128) DEFAULT NULL,
  `ip_address` varchar(32) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table sys_exchage_rate_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_exchage_rate_log`;

CREATE TABLE `sys_exchage_rate_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform` varchar(16) DEFAULT NULL COMMENT '币价平台',
  `coin_symbol` varchar(20) NOT NULL DEFAULT '' COMMENT '定价币种',
  `to_coin_symbol` varchar(50) DEFAULT NULL COMMENT '被定价币种',
  `rate` decimal(18,8) DEFAULT NULL COMMENT '汇率',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `memo` varchar(256) DEFAULT NULL COMMENT '备注',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`,`coin_symbol`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table sys_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '名称',
  `pid` bigint(20) DEFAULT NULL COMMENT '父级id',
  `level` int(11) DEFAULT NULL COMMENT '级别 1-一级 2-二级 3-三级',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  `logo_url` varchar(255) DEFAULT NULL COMMENT 'logo',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table sys_request_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_request_log`;

CREATE TABLE `sys_request_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `channel_domain` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `device_uuid` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error_code` int(11) DEFAULT NULL COMMENT '访问密钥',
  `request_uuid` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求唯一编号',
  `method` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求方式',
  `module` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求时间毫秒值',
  `request_ip` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '请求参数',
  `request_parmas` text COLLATE utf8mb4_unicode_ci COMMENT '签名',
  `request_uri` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response` longtext COLLATE utf8mb4_unicode_ci,
  `request_url` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_time` bigint(20) DEFAULT NULL COMMENT '响应',
  `stack_trace` text COLLATE utf8mb4_unicode_ci COMMENT '请求地址',
  `timestamp` bigint(20) DEFAULT NULL COMMENT 'ip',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idex_srl_request_uuid` (`request_uuid`) USING BTREE,
  KEY `idx_hrl_customer_id` (`customer_id`) USING BTREE,
  KEY `idx_hrl_device_log` (`device_uuid`) USING BTREE,
  KEY `idx_hrl_method` (`method`) USING BTREE,
  KEY `idx_hrl_created_at` (`created_at`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table sys_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(32) DEFAULT NULL COMMENT '角色名',
  `op_uid` bigint(20) DEFAULT NULL COMMENT '操作员ID',
  `remark` varchar(30) NOT NULL DEFAULT '' COMMENT '备注',
  `status` int(8) DEFAULT '0' COMMENT '状态 -1-删除 0-启用 1-禁用',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='要更新SysRoleType信息';



# Dump of table sys_role_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sys_role_id` bigint(20) DEFAULT NULL,
  `menu_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table sys_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `moblie` varchar(20) DEFAULT NULL COMMENT '手机号',
  `password` varchar(256) DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(64) DEFAULT NULL COMMENT '昵称',
  `wallet_address` varchar(64) DEFAULT NULL COMMENT '钱包地址',
  `sys_role_id` bigint(20) DEFAULT NULL COMMENT '系统角色ID',
  `icon_url` varchar(128) CHARACTER SET ascii DEFAULT NULL COMMENT '头像',
  `op_uid` bigint(20) DEFAULT NULL COMMENT '操作员ID',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  `status` int(8) DEFAULT '0' COMMENT '状态 -1-删除 0-正常 1-冻结',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sys_user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sys_uid` bigint(20) DEFAULT NULL COMMENT '系统用户UID',
  `sys_role_id` bigint(20) DEFAULT NULL COMMENT '系统角色ID',
  `op_uid` bigint(20) DEFAULT NULL COMMENT '操作员ID',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table token
# ------------------------------------------------------------

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contract_reg_id` varchar(32) DEFAULT NULL COMMENT '合约注册ID',
  `token_name` varchar(32) DEFAULT NULL COMMENT '代币名称',
  `token_symbol` varchar(32) DEFAULT NULL COMMENT '代币简称',
  `standard` varchar(128) DEFAULT NULL COMMENT '遵循的标准',
  `last_sync_height` int(11) DEFAULT '0' COMMENT '最后同步高度默为0 ，初始化为发币高度',
  `create_address` varchar(64) DEFAULT NULL COMMENT '创建地址',
  `contract_address` varchar(64) DEFAULT NULL COMMENT '合约地址',
  `total_supply_count` decimal(32,8) DEFAULT NULL COMMENT '发币总数',
  `mintable` tinyint(1) DEFAULT NULL COMMENT '是否增发',
  `create_hash` varchar(255) DEFAULT NULL COMMENT 'create hash id',
  `confirmed_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `description_cn` varchar(1024) DEFAULT NULL COMMENT '描述',
  `description_en` varchar(1024) DEFAULT NULL COMMENT '描述',
  `logo_url` varchar(256) DEFAULT NULL COMMENT 'logo',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_token_symbol` (`token_symbol`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table token_balance
# ------------------------------------------------------------

DROP TABLE IF EXISTS `token_balance`;

CREATE TABLE `token_balance` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `wallet_address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '钱包地址',
  `token_contract_address` varchar(128) DEFAULT NULL COMMENT 'token_ico_address',
  `token_reg_id` varchar(64) DEFAULT NULL COMMENT 'token_ico_regid',
  `token_symbol` varchar(32) DEFAULT NULL COMMENT 'token名字',
  `balance` decimal(32,8) DEFAULT '0.00000000' COMMENT '余额',
  `trancation_count` bigint(20) DEFAULT NULL COMMENT '交易数量',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `standard` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_tb_address` (`wallet_address`) USING BTREE,
  KEY `id_wallet_address` (`wallet_address`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table token_stats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `token_stats`;

CREATE TABLE `token_stats` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stats_date` varchar(32) DEFAULT NULL COMMENT '统计日期',
  `token_contract_address` varchar(128) DEFAULT NULL COMMENT 'token合约地址',
  `holder_count` int(11) DEFAULT NULL COMMENT '持币地址数',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_ts_statsdate` (`stats_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table token_transaction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `token_transaction`;

CREATE TABLE `token_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `token_symbol` varchar(16) DEFAULT NULL COMMENT '代币标识',
  `token_contract_address` varchar(128) DEFAULT NULL COMMENT 'ico合约地址',
  `token_reg_id` varchar(32) DEFAULT NULL,
  `tx_hash` varchar(128) DEFAULT NULL COMMENT '交易号',
  `src_address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '付款地址',
  `dest_address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '收款地址',
  `fee` decimal(32,8) DEFAULT NULL COMMENT '手续费',
  `amount` decimal(32,8) DEFAULT NULL COMMENT '金额',
  `height` int(11) DEFAULT NULL COMMENT '交易提交高度',
  `confirm_height` int(11) DEFAULT NULL COMMENT '交易确认高度',
  `transacted_at` datetime DEFAULT NULL COMMENT '交易时间',
  `tx_remark` varchar(512) DEFAULT NULL COMMENT '交易备注',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_tx_hash` (`tx_hash`) USING BTREE,
  KEY `src_address` (`src_address`) USING BTREE,
  KEY `idx_dest_address` (`dest_address`) USING BTREE,
  KEY `idx_confirm_height` (`confirm_height`) USING BTREE,
  KEY `idx_tran_at` (`transacted_at`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table wallet_balance
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wallet_balance`;

CREATE TABLE `wallet_balance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `coin_symbol` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `coin_amount` bigint(255) DEFAULT NULL,
  `transaction_count` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `un_address_coinSymbol` (`address`,`coin_symbol`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table wicc_block
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wicc_block`;

CREATE TABLE `wicc_block` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `height` int(11) DEFAULT NULL COMMENT '区块号	',
  `hash` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '块hash',
  `confirmations` int(11) DEFAULT NULL COMMENT '确认次数',
  `size` int(11) DEFAULT NULL COMMENT '快大小 （KB）',
  `transaction_count` int(11) DEFAULT NULL COMMENT '交易笔数',
  `transaction_amount` decimal(32,8) DEFAULT NULL COMMENT '总交易金额',
  `transaction_fees` decimal(32,8) DEFAULT NULL COMMENT '总交易手续费',
  `version` int(11) DEFAULT NULL COMMENT '哈希值',
  `merkle_root` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '梅克来根hash',
  `produced_at` datetime DEFAULT NULL COMMENT '创建时间',
  `nonce` bigint(20) DEFAULT NULL COMMENT '随机数',
  `chainwork` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fuel` decimal(32,8) DEFAULT NULL COMMENT '燃料',
  `fuel_rate` int(11) DEFAULT NULL COMMENT '燃料率',
  `miner_address` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '矿工地址',
  `miner_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '矿工名字',
  `previous_block_hash` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上一区块hash',
  `next_block_hash` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '下一区块hash',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_bwb_number` (`height`) USING BTREE,
  KEY `idx_bwb_hash` (`hash`) USING BTREE,
  KEY `idx_bwb_time` (`produced_at`) USING BTREE,
  KEY `idx_updated_at` (`updated_at`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;



# Dump of table wicc_chain_stats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wicc_chain_stats`;

CREATE TABLE `wicc_chain_stats` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stats_date` varchar(32) DEFAULT NULL,
  `new_address_count` int(11) DEFAULT NULL,
  `new_tx_count` int(11) DEFAULT NULL,
  `total_address_account` int(11) DEFAULT NULL,
  `wicc_next_end_price` decimal(20,8) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_wcs_stats_date` (`stats_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table wicc_market_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wicc_market_info`;

CREATE TABLE `wicc_market_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `coin_symbol` varchar(32) DEFAULT NULL COMMENT '代币名称: WICC',
  `current_price` decimal(32,8) DEFAULT NULL COMMENT '当前价格',
  `volume_24h` decimal(32,8) DEFAULT NULL COMMENT '24小时交易量',
  `percent_change_24h` decimal(10,2) DEFAULT NULL COMMENT '24小时涨跌幅',
  `rank` int(11) DEFAULT NULL COMMENT '全球排名',
  `market_cap` decimal(32,8) DEFAULT NULL COMMENT '市场流通量',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table wicc_rollbacked_block
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wicc_rollbacked_block`;

CREATE TABLE `wicc_rollbacked_block` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL COMMENT '区块号	',
  `hash` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '块hash',
  `confirmations` int(11) DEFAULT NULL COMMENT '确认次数',
  `size` int(11) DEFAULT NULL COMMENT '快大小 （KB）',
  `height` int(11) DEFAULT NULL COMMENT '高度',
  `transaction_count` int(11) DEFAULT NULL COMMENT '交易笔数',
  `transaction_amount` decimal(32,8) DEFAULT NULL COMMENT '总交易金额',
  `transaction_fees` decimal(32,8) DEFAULT NULL COMMENT '总交易手续费',
  `version` int(11) DEFAULT NULL COMMENT '哈希值',
  `merkle_root` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '梅克来根hash',
  `produced_at` datetime DEFAULT NULL COMMENT '创建时间',
  `nonce` bigint(20) DEFAULT NULL COMMENT '随机数',
  `chainwork` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fuel` decimal(32,8) DEFAULT NULL COMMENT '燃料',
  `fuel_rate` int(11) DEFAULT NULL COMMENT '燃料率',
  `miner_address` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '矿工地址',
  `miner_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '矿工名字',
  `previous_block_hash` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '上一区块hash',
  `next_block_hash` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '下一区块hash',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_bwb_number` (`number`) USING BTREE,
  KEY `idx_bwb_hash` (`hash`) USING BTREE,
  KEY `idx_bwb_time` (`produced_at`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;



# Dump of table wicc_transaction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wicc_transaction`;

CREATE TABLE `wicc_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tx_hash` varchar(64) DEFAULT NULL COMMENT '交易hash',
  `coin_symbol` varchar(32) DEFAULT NULL COMMENT '钱包货币符号 WICC',
  `tx_type` varchar(64) DEFAULT NULL COMMENT '交易类型(1系统分红，2钱包注册，3普通转账，4合约交易，5合约发布，6投票交易)',
  `src_address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发送地址',
  `dest_address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '目标地址',
  `src_uid` varchar(128) DEFAULT NULL COMMENT '钱包注册id',
  `dest_uid` varchar(128) DEFAULT NULL COMMENT '钱包接收者注册id',
  `tx_amount` decimal(32,8) DEFAULT NULL COMMENT '转账金额',
  `fee` decimal(32,8) DEFAULT NULL COMMENT '转账小费',
  `fee_symbol` varchar(32) DEFAULT NULL COMMENT '小费货币符号 WICC',
  `transaction_at` datetime DEFAULT NULL COMMENT '交易完成时间',
  `confirm_block_hash` varchar(128) DEFAULT NULL COMMENT '确认交易的区块hash',
  `confirm_height` int(11) DEFAULT NULL COMMENT '交易区块高度',
  `valid_height` int(11) DEFAULT NULL COMMENT '提交高度',
  `remark` text COMMENT '交易备注',
  `contract` text COMMENT '合约内容',
  `raw_tx` mediumtext COMMENT '交易签名数据',
  `receipt` mediumtext COMMENT '收据',
  `tx_content` mediumtext COMMENT '完整交易信息',
  `rollbacked` tinyint(1) DEFAULT '0' COMMENT '是否被回滚了',
  `tx_type_code` int(11) DEFAULT '0' COMMENT '交易类型数字标记',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `asset_symbol` varchar(32) DEFAULT NULL COMMENT '代币符号',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_wtr_tx_hash` (`tx_hash`) USING BTREE,
  KEY `idx_wt_sw` (`src_address`) USING BTREE,
  KEY `idx_wt_dw` (`dest_address`) USING BTREE,
  KEY `idx_wt_bh` (`confirm_height`) USING BTREE,
  KEY `idx_tx_type` (`tx_type`) USING BTREE,
  KEY `idx_wt_cbh` (`confirm_block_hash`) USING BTREE,
  KEY `idx_wt_transaction_at` (`transaction_at`) USING BTREE,
  KEY `idx_type_src` (`tx_type`,`src_address`) USING BTREE,
  KEY `idx_sw_dw_symbol` (`coin_symbol`,`src_address`,`dest_address`) USING BTREE,
  KEY `idx_rb_type_addr` (`rollbacked`,`tx_type`,`src_address`,`dest_address`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;



# Dump of table wicc_vote_transaction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wicc_vote_transaction`;

CREATE TABLE `wicc_vote_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tx_hash` varchar(64) DEFAULT NULL COMMENT '交易hash',
  `vote_type` varchar(64) DEFAULT NULL COMMENT '交易类型(ADD_BCOIN-增票 MINUS_FUND-减票)',
  `src_address` varchar(128) DEFAULT NULL COMMENT '发送地址',
  `dest_address` varchar(128) DEFAULT NULL COMMENT '目标地址',
  `src_uid` varchar(128) DEFAULT NULL COMMENT '钱包注册id',
  `dest_uid` varchar(128) DEFAULT NULL COMMENT '钱包接收者注册id',
  `voted_bcoins` decimal(32,8) DEFAULT NULL COMMENT '投票金额',
  `coin_symbol` varchar(32) DEFAULT NULL COMMENT '钱包货币符号 WICC',
  `fee` decimal(32,8) DEFAULT NULL COMMENT '转账小费',
  `fee_symbol` varchar(32) DEFAULT NULL COMMENT '小费货币符号 WICC',
  `candidate_type` varchar(32) DEFAULT NULL COMMENT '被投地址标识类型',
  `candidate_id` varchar(128) DEFAULT NULL COMMENT '被投地址标识类型',
  `block_hash` varchar(128) DEFAULT NULL COMMENT '确认交易的区块hash',
  `valid_height` int(11) DEFAULT NULL COMMENT '提交高度',
  `confirm_height` int(11) DEFAULT NULL COMMENT '交易区块高度',
  `confirmed_time` bigint(20) DEFAULT NULL COMMENT '交易链上确认时间',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_tixd_addr_toaddr` (`tx_hash`,`src_address`,`dest_address`) USING BTREE,
  KEY `idx_wtr_tx_hash` (`tx_hash`) USING BTREE,
  KEY `idx_wt_sw` (`src_address`) USING BTREE,
  KEY `idx_wt_dw` (`dest_address`) USING BTREE,
  KEY `idx_wt_bh` (`confirm_height`) USING BTREE,
  KEY `idx_tx_type` (`vote_type`) USING BTREE,
  KEY `idx_wt_cbh` (`block_hash`) USING BTREE,
  KEY `idx_wt_confirmed_time` (`confirmed_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;



# Dump of table wicc_wallet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wicc_wallet`;

CREATE TABLE `wicc_wallet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `reg_id` varchar(11) DEFAULT NULL COMMENT '注册id',
  `address_name` varchar(128) DEFAULT NULL COMMENT '地址名称',
  `nick_id` varchar(128) DEFAULT NULL COMMENT '昵称',
  `address_type` int(11) DEFAULT '100' COMMENT '地址类型 100普通地址。200合约地址',
  `key_id` varchar(128) DEFAULT NULL COMMENT '私钥编号',
  `real_created_at` datetime DEFAULT NULL,
  `publickey` varchar(100) DEFAULT NULL COMMENT '公钥',
  `wicc_balance` decimal(36,8) DEFAULT NULL,
  `wusd_balance` decimal(36,8) DEFAULT NULL COMMENT 'wusd 余额',
  `wgrt_balance` decimal(36,8) DEFAULT NULL COMMENT 'wgrt 余额',
  `wicc_balance_staked` decimal(36,8) DEFAULT NULL,
  `wicc_balance_frozen` decimal(36,8) DEFAULT NULL,
  `wicc_balance_voted` decimal(36,8) DEFAULT NULL,
  `wicc_balance_pledged` decimal(36,8) DEFAULT NULL COMMENT 'CDP抵押',
  `wicc_balance_total` decimal(36,8) DEFAULT NULL COMMENT 'WICC total',
  `wusd_balance_staked` decimal(36,8) DEFAULT NULL,
  `wusd_balance_frozen` decimal(36,8) DEFAULT NULL,
  `wusd_balance_voted` decimal(36,8) DEFAULT NULL,
  `wusd_balance_pledged` decimal(36,8) DEFAULT NULL,
  `wusd_balance_total` decimal(36,8) DEFAULT NULL,
  `wgrt_balance_staked` decimal(36,8) DEFAULT NULL,
  `wgrt_balance_frozen` decimal(36,8) DEFAULT NULL,
  `wgrt_balance_voted` decimal(36,8) DEFAULT NULL,
  `wgrt_balance_total` decimal(36,8) DEFAULT NULL,
  `tokens` text COMMENT '资产列表',
  `reg_txid` varchar(100) DEFAULT NULL COMMENT '激活时交易hash',
  `miner_pubkey` varchar(256) DEFAULT NULL COMMENT '旷工公钥',
  `transaction_count` int(11) DEFAULT '0' COMMENT '交易总数',
  `last_stats_height` int(11) DEFAULT '0' COMMENT '最后统计高度',
  `vote_number` decimal(32,8) DEFAULT '0.00000000' COMMENT '获票数量',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `ww_address` (`address`) USING BTREE,
  UNIQUE KEY `idx_ww_address` (`address`) USING BTREE,
  KEY `ww_idx_balance` (`wicc_balance`) USING BTREE,
  KEY `idx_up` (`updated_at`) USING BTREE,
  KEY `idx_wusd_balance` (`wusd_balance`) USING BTREE COMMENT 'wusd blance index'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
