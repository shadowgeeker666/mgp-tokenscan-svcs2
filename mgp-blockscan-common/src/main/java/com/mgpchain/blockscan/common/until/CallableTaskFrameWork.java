package com.mgpchain.blockscan.common.until;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author : CHEN
 * @Date : Created in 2021/7/2
 * @description：
 * @modified By：
 **/
public class CallableTaskFrameWork implements ICallableTaskFrameWork {

    private IConcurrentThreadPool concurrentThreadPool;

    public CallableTaskFrameWork(ConcurrentThreadPool concurrentThreadPool) {
        this.concurrentThreadPool = concurrentThreadPool;
    }


    @Override
    public <V> List<V> submitsAll(List<? extends CallableTemplate<V>> tasks)
            throws InterruptedException, ExecutionException {
        return concurrentThreadPool.invokeAll(tasks);
    }
}
