package com.mgpchain.blockscan.common.until;

import java.util.concurrent.Callable;

/**
 * @author : CHEN
 * @Date : Created in 2021/7/2
 * @description：多线程模板类
 * @modified By：
 **/


public abstract class CallableTemplate<V> implements Callable<V> {

    /**
     * 前置处理，子类可以Override该方法
     */

    public void beforeProcess() {
    }

    /**
     * 处理业务逻辑的方法,需要子类去Override
     *
     * @param <>
     * @return
     */

    public abstract V process();

    /**
     * 后置处理，子类可以Override该方法
     */

    public void afterProcess() {
    }

    @Override

    public V call() throws Exception {

        beforeProcess();

        V result = process();

        afterProcess();

        return result;

    }

}