package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.MbAccount

interface MbAccountService {
    fun getById(id:Long): MbAccount?

    fun save(mbAccount:MbAccount): MbAccount
    fun genAccount(name: String, blockNum: Long): MbAccount
    fun addSyncFlag(vararg name: String)
    fun findBySyncFlagGreaterThan(i: Int): List<MbAccount>?
    fun getByName(name: String): MbAccount?
}
