package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.SysConfigService
import com.mgpchain.blockscan.entity.domain.SysConfig
import com.mgpchain.blockscan.repository.SysConfigRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class SysConfigServiceImpl: SysConfigService {

    override fun getById(id:Long): SysConfig? {
        return sysConfigRepository.findOne(id)
    }

    override fun save(sysConfig:SysConfig): SysConfig {
        return sysConfigRepository.saveAndFlush(sysConfig)
    }

    override fun findByCfgName(name: String): String {
        return sysConfigRepository.findByName(name)
    }

    @Autowired lateinit var sysConfigRepository: SysConfigRepository
}
