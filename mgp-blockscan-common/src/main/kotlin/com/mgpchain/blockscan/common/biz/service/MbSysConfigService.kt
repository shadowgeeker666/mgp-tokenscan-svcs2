package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.MbSysConfig

interface MbSysConfigService {
    fun getById(id:Long): MbSysConfig?

    fun save(mbSysConfig:MbSysConfig): MbSysConfig
    fun getByName(cfgName: String): MbSysConfig
}
