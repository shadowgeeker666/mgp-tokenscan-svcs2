package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.BpVoteNode

interface BpVoteNodeService {
    fun getById(id:Long): BpVoteNode?

    fun save(bpVoteNode:BpVoteNode): BpVoteNode
}
