package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.MbActions

interface MbActionsService {
    fun getById(id:Long): MbActions?

    fun save(mbActions:MbActions): MbActions

    fun save(mbActions: List<MbActions>)
}
