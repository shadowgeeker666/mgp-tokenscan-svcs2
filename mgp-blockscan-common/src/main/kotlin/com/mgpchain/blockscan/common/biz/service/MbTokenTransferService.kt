package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.MbTokenTransfer

interface MbTokenTransferService {
    fun getById(id:Long): MbTokenTransfer?

    fun save(mbTokenTransfer:MbTokenTransfer): MbTokenTransfer
}
