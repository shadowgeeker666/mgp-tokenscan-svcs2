package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.MbActionsService
import com.mgpchain.blockscan.entity.domain.MbActions
import com.mgpchain.blockscan.repository.MbActionsRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class MbActionsServiceImpl: MbActionsService {

    override fun getById(id:Long): MbActions? {
        return mbActionsRepository.findOne(id)
    }

    override fun save(mbActions:MbActions): MbActions {
        return mbActionsRepository.saveAndFlush(mbActions)
    }

    override fun save(mbActions: List<MbActions>) {
        mbActionsRepository.save(mbActions)
    }

    @Autowired lateinit var mbActionsRepository: MbActionsRepository
}
