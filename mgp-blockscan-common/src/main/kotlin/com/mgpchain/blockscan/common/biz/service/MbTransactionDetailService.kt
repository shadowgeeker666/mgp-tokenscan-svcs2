package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.MbTransactionDetail

interface MbTransactionDetailService {
    fun getById(id: Long): MbTransactionDetail?

    fun save(mbTransactionDetail: MbTransactionDetail): MbTransactionDetail

    fun savaAll(mbTransactionDetails: List<MbTransactionDetail>)
}
