package com.mgpchain.blockscan.common.biz.env

import org.joda.time.DateTimeZone
import java.math.BigDecimal
import java.util.*

object BaseEnv {
    fun env(name: String, value: String): String {
        return System.getenv(name) ?: value
    }

    fun init() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+0"))
        DateTimeZone.setDefault(DateTimeZone.forOffsetHours(0))
    }

    fun env(name: String, value: Int): Int {
        return System.getenv(name)?.toInt() ?: value
    }

    fun env(name: String, value: Long): Long {
        return System.getenv(name)?.toLong() ?: value
    }

    fun env(name: String, value: Boolean): Boolean {
        return System.getenv(name)?.toBoolean() ?: value
    }

    fun env(name: String, value: Float): Float {
        return System.getenv(name)?.toFloat() ?: value
    }

    fun env(name: String, value: BigDecimal): BigDecimal {
        return System.getenv(name)?.toBigDecimal() ?: value
    }
}