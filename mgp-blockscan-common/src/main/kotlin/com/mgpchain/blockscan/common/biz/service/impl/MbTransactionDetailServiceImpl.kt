package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.MbTransactionDetailService
import com.mgpchain.blockscan.entity.domain.MbTransactionDetail
import com.mgpchain.blockscan.repository.MbTransactionDetailRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class MbTransactionDetailServiceImpl : MbTransactionDetailService {

    override fun getById(id: Long): MbTransactionDetail? {
        return mbTransactionDetailRepository.findOne(id)
    }

    override fun save(mbTransactionDetail: MbTransactionDetail): MbTransactionDetail {
        return mbTransactionDetailRepository.saveAndFlush(mbTransactionDetail)
    }

    override fun savaAll(mbTransactionDetails: List<MbTransactionDetail>) {
        mbTransactionDetailRepository.save(mbTransactionDetails)
    }

    @Autowired
    lateinit var mbTransactionDetailRepository: MbTransactionDetailRepository

}
