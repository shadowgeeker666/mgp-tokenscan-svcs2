package com.mgpchain.blockscan.common.biz.service

import com.mgpchain.blockscan.entity.domain.MbBlockTransaction

interface MbBlockTransactionService {
    fun getById(id:Long): MbBlockTransaction?

    fun save(mbBlockTransaction:MbBlockTransaction): MbBlockTransaction
    fun findMaxBlockNum(): Long
}
