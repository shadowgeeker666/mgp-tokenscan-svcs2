package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.BpVoteNodeService
import com.mgpchain.blockscan.entity.domain.BpVoteNode
import com.mgpchain.blockscan.repository.BpVoteNodeRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class BpVoteNodeServiceImpl: BpVoteNodeService {

    override fun getById(id:Long): BpVoteNode? {
        return bpVoteNodeRepository.findOne(id)
    }

    override fun save(bpVoteNode:BpVoteNode): BpVoteNode {
        return bpVoteNodeRepository.saveAndFlush(bpVoteNode)
    }

    @Autowired lateinit var bpVoteNodeRepository: BpVoteNodeRepository
}
