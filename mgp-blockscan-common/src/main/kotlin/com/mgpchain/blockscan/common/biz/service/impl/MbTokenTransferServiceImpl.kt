package com.mgpchain.blockscan.common.biz.service.impl

import com.mgpchain.blockscan.common.biz.service.MbTokenTransferService
import com.mgpchain.blockscan.entity.domain.MbTokenTransfer
import com.mgpchain.blockscan.repository.MbTokenTransferRepository
import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired

@Service
class MbTokenTransferServiceImpl: MbTokenTransferService {

    override fun getById(id:Long): MbTokenTransfer? {
        return mbTokenTransferRepository.findOne(id)
    }

    override fun save(mbTokenTransfer:MbTokenTransfer): MbTokenTransfer {
        return mbTokenTransferRepository.saveAndFlush(mbTokenTransfer)
    }

    @Autowired lateinit var mbTokenTransferRepository: MbTokenTransferRepository
}
