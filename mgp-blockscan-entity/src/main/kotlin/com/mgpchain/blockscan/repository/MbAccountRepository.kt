package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.MbAccount
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface MbAccountRepository : JpaRepository<MbAccount, Long>,
    QueryDslPredicateExecutor<MbAccount> {
    fun findByAccount(it: String): MbAccount?
    fun findBySyncFlagGreaterThan(i: Int) : List<MbAccount>?
}
