package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.MbRequestLog
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface MbRequestLogRepository : JpaRepository<MbRequestLog, Long>,
    QueryDslPredicateExecutor<MbRequestLog>
