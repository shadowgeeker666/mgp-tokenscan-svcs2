package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.MbBlockTransaction
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface MbBlockTransactionRepository : JpaRepository<MbBlockTransaction, Long>,
    QueryDslPredicateExecutor<MbBlockTransaction>