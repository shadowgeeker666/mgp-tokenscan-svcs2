package com.mgpchain.blockscan.repository

import com.mgpchain.blockscan.entity.domain.MbTransactionDetail
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface MbTransactionDetailRepository : JpaRepository<MbTransactionDetail, Long>,
    QueryDslPredicateExecutor<MbTransactionDetail>
