package com.mgpchain.blockscan.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * MbBlock is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class MbBlock implements Serializable {

    @Column("action_mroot")
    private String actionMroot;

    @Column("block_extensions")
    private String blockExtensions;

    @Column("block_id")
    private String blockId;

    @Column("block_num")
    private Long blockNum;

    @Column("confirmed")
    private Integer confirmed;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("header_extensions")
    private String headerExtensions;

    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    @Column("id")
    private Long id;

    @Column("new_producers")
    private String newProducers;

    @Column("previous")
    private String previous;

    @Column("producer")
    private String producer;

    @Column("producer_signature")
    private String producerSignature;

    @Column("ref_block_prefix")
    private Long refBlockPrefix;

    @Column("schedule_version")
    private Integer scheduleVersion;

    @Column("timestamp")
    private java.util.Date timestamp;

    @Column("transaction_mroot")
    private String transactionMroot;

    @Column("transactions")
    private String transactions;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public String getActionMroot() {
        return actionMroot;
    }

    public void setActionMroot(String actionMroot) {
        this.actionMroot = actionMroot;
    }

    public String getBlockExtensions() {
        return blockExtensions;
    }

    public void setBlockExtensions(String blockExtensions) {
        this.blockExtensions = blockExtensions;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public Long getBlockNum() {
        return blockNum;
    }

    public void setBlockNum(Long blockNum) {
        this.blockNum = blockNum;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getHeaderExtensions() {
        return headerExtensions;
    }

    public void setHeaderExtensions(String headerExtensions) {
        this.headerExtensions = headerExtensions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNewProducers() {
        return newProducers;
    }

    public void setNewProducers(String newProducers) {
        this.newProducers = newProducers;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getProducerSignature() {
        return producerSignature;
    }

    public void setProducerSignature(String producerSignature) {
        this.producerSignature = producerSignature;
    }

    public Long getRefBlockPrefix() {
        return refBlockPrefix;
    }

    public void setRefBlockPrefix(Long refBlockPrefix) {
        this.refBlockPrefix = refBlockPrefix;
    }

    public Integer getScheduleVersion() {
        return scheduleVersion;
    }

    public void setScheduleVersion(Integer scheduleVersion) {
        this.scheduleVersion = scheduleVersion;
    }

    public java.util.Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(java.util.Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getTransactionMroot() {
        return transactionMroot;
    }

    public void setTransactionMroot(String transactionMroot) {
        this.transactionMroot = transactionMroot;
    }

    public String getTransactions() {
        return transactions;
    }

    public void setTransactions(String transactions) {
        this.transactions = transactions;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}

