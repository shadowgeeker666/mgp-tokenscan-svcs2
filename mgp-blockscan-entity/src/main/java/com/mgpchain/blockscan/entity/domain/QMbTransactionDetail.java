package com.mgpchain.blockscan.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMbTransactionDetail is a Querydsl query type for MbTransactionDetail
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QMbTransactionDetail extends com.querydsl.sql.RelationalPathBase<MbTransactionDetail> {

    private static final long serialVersionUID = -1311065279;

    public static final QMbTransactionDetail mbTransactionDetail = new QMbTransactionDetail("mb_transaction_detail");

    public final StringPath account = createString("account");

    public final NumberPath<java.math.BigDecimal> amount = createNumber("amount", java.math.BigDecimal.class);

    public final NumberPath<Long> blockNum = createNumber("blockNum", Long.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath fromAccount = createString("fromAccount");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath memo = createString("memo");

    public final StringPath quantity = createString("quantity");

    public final StringPath symbol = createString("symbol");

    public final StringPath toAccount = createString("toAccount");

    public final StringPath transactionId = createString("transactionId");

    public final StringPath txName = createString("txName");

    public final DateTimePath<java.util.Date> txTime = createDateTime("txTime", java.util.Date.class);

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<MbTransactionDetail> primary = createPrimaryKey(id);

    public QMbTransactionDetail(String variable) {
        super(MbTransactionDetail.class, forVariable(variable), "null", "mb_transaction_detail");
        addMetadata();
    }

    public QMbTransactionDetail(String variable, String schema, String table) {
        super(MbTransactionDetail.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMbTransactionDetail(String variable, String schema) {
        super(MbTransactionDetail.class, forVariable(variable), schema, "mb_transaction_detail");
        addMetadata();
    }

    public QMbTransactionDetail(Path<? extends MbTransactionDetail> path) {
        super(path.getType(), path.getMetadata(), "null", "mb_transaction_detail");
        addMetadata();
    }

    public QMbTransactionDetail(PathMetadata metadata) {
        super(MbTransactionDetail.class, metadata, "null", "mb_transaction_detail");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(account, ColumnMetadata.named("account").withIndex(12).ofType(Types.VARCHAR).withSize(255));
        addMetadata(amount, ColumnMetadata.named("amount").withIndex(7).ofType(Types.DECIMAL).withSize(26).withDigits(8));
        addMetadata(blockNum, ColumnMetadata.named("block_num").withIndex(2).ofType(Types.BIGINT).withSize(19));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(13).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(fromAccount, ColumnMetadata.named("from_account").withIndex(5).ofType(Types.VARCHAR).withSize(255));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(memo, ColumnMetadata.named("memo").withIndex(10).ofType(Types.VARCHAR).withSize(255));
        addMetadata(quantity, ColumnMetadata.named("quantity").withIndex(9).ofType(Types.VARCHAR).withSize(255));
        addMetadata(symbol, ColumnMetadata.named("symbol").withIndex(8).ofType(Types.VARCHAR).withSize(255));
        addMetadata(toAccount, ColumnMetadata.named("to_account").withIndex(6).ofType(Types.VARCHAR).withSize(255));
        addMetadata(transactionId, ColumnMetadata.named("transaction_id").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(txName, ColumnMetadata.named("tx_name").withIndex(11).ofType(Types.VARCHAR).withSize(255));
        addMetadata(txTime, ColumnMetadata.named("tx_time").withIndex(4).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(14).ofType(Types.TIMESTAMP).withSize(19));
    }

}

