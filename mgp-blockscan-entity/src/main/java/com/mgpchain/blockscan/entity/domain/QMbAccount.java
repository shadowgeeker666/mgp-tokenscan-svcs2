package com.mgpchain.blockscan.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMbAccount is a Querydsl query type for MbAccount
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QMbAccount extends com.querydsl.sql.RelationalPathBase<MbAccount> {

    private static final long serialVersionUID = 984978207;

    public static final QMbAccount mbAccount = new QMbAccount("mb_account");

    public final StringPath account = createString("account");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<Long> createdBlockNum = createNumber("createdBlockNum", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> staked = createNumber("staked", java.math.BigDecimal.class);

    public final NumberPath<Integer> syncFlag = createNumber("syncFlag", Integer.class);

    public final NumberPath<java.math.BigDecimal> totalBalance = createNumber("totalBalance", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> unstaked = createNumber("unstaked", java.math.BigDecimal.class);

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<MbAccount> primary = createPrimaryKey(id);

    public QMbAccount(String variable) {
        super(MbAccount.class, forVariable(variable), "null", "mb_account");
        addMetadata();
    }

    public QMbAccount(String variable, String schema, String table) {
        super(MbAccount.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMbAccount(String variable, String schema) {
        super(MbAccount.class, forVariable(variable), schema, "mb_account");
        addMetadata();
    }

    public QMbAccount(Path<? extends MbAccount> path) {
        super(path.getType(), path.getMetadata(), "null", "mb_account");
        addMetadata();
    }

    public QMbAccount(PathMetadata metadata) {
        super(MbAccount.class, metadata, "null", "mb_account");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(account, ColumnMetadata.named("account").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(6).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(createdBlockNum, ColumnMetadata.named("created_block_num").withIndex(3).ofType(Types.BIGINT).withSize(19));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(staked, ColumnMetadata.named("staked").withIndex(8).ofType(Types.DECIMAL).withSize(26).withDigits(8));
        addMetadata(syncFlag, ColumnMetadata.named("sync_flag").withIndex(5).ofType(Types.INTEGER).withSize(10));
        addMetadata(totalBalance, ColumnMetadata.named("total_balance").withIndex(4).ofType(Types.DECIMAL).withSize(26).withDigits(8));
        addMetadata(unstaked, ColumnMetadata.named("unstaked").withIndex(9).ofType(Types.DECIMAL).withSize(26).withDigits(8));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(7).ofType(Types.TIMESTAMP).withSize(19));
    }

}

