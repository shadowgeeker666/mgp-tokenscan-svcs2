package com.mgpchain.blockscan.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMbTokenTransfer is a Querydsl query type for MbTokenTransfer
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QMbTokenTransfer extends com.querydsl.sql.RelationalPathBase<MbTokenTransfer> {

    private static final long serialVersionUID = 242276470;

    public static final QMbTokenTransfer mbTokenTransfer = new QMbTokenTransfer("mb_token_transfer");

    public final StringPath action = createString("action");

    public final NumberPath<java.math.BigDecimal> amount = createNumber("amount", java.math.BigDecimal.class);

    public final StringPath contract = createString("contract");

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final StringPath fromAccount = createString("fromAccount");

    public final NumberPath<Long> height = createNumber("height", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath memo = createString("memo");

    public final StringPath symbol = createString("symbol");

    public final StringPath toAccount = createString("toAccount");

    public final StringPath txid = createString("txid");

    public final DateTimePath<java.util.Date> txtime = createDateTime("txtime", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<MbTokenTransfer> primary = createPrimaryKey(id);

    public QMbTokenTransfer(String variable) {
        super(MbTokenTransfer.class, forVariable(variable), "null", "mb_token_transfer");
        addMetadata();
    }

    public QMbTokenTransfer(String variable, String schema, String table) {
        super(MbTokenTransfer.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMbTokenTransfer(String variable, String schema) {
        super(MbTokenTransfer.class, forVariable(variable), schema, "mb_token_transfer");
        addMetadata();
    }

    public QMbTokenTransfer(Path<? extends MbTokenTransfer> path) {
        super(path.getType(), path.getMetadata(), "null", "mb_token_transfer");
        addMetadata();
    }

    public QMbTokenTransfer(PathMetadata metadata) {
        super(MbTokenTransfer.class, metadata, "null", "mb_token_transfer");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(action, ColumnMetadata.named("action").withIndex(6).ofType(Types.VARCHAR).withSize(255));
        addMetadata(amount, ColumnMetadata.named("amount").withIndex(9).ofType(Types.DECIMAL).withSize(26).withDigits(8));
        addMetadata(contract, ColumnMetadata.named("contract").withIndex(5).ofType(Types.VARCHAR).withSize(255));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(12).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(fromAccount, ColumnMetadata.named("from_account").withIndex(7).ofType(Types.VARCHAR).withSize(255));
        addMetadata(height, ColumnMetadata.named("height").withIndex(2).ofType(Types.BIGINT).withSize(19));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(memo, ColumnMetadata.named("memo").withIndex(11).ofType(Types.VARCHAR).withSize(255));
        addMetadata(symbol, ColumnMetadata.named("symbol").withIndex(10).ofType(Types.VARCHAR).withSize(255));
        addMetadata(toAccount, ColumnMetadata.named("to_account").withIndex(8).ofType(Types.VARCHAR).withSize(255));
        addMetadata(txid, ColumnMetadata.named("txid").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(txtime, ColumnMetadata.named("txtime").withIndex(4).ofType(Types.TIMESTAMP).withSize(19));
    }

}

