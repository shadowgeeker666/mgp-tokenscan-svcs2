package com.mgpchain.blockscan.entity.domain;

import javax.persistence.Entity;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.DynamicUpdate;
import com.querydsl.sql.Column;
import org.hibernate.annotations.DynamicInsert;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * MbAccount is a Querydsl bean type
 */
@Entity
@DynamicInsert
@DynamicUpdate
public class MbAccount implements Serializable {

    @Column("account")
    private String account;

    @Column("created_at")
    private java.util.Date createdAt;

    @Column("created_block_num")
    private Long createdBlockNum;

    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Id
    @Column("id")
    private Long id;

    @Column("staked")
    private java.math.BigDecimal staked;

    @Column("sync_flag")
    private Integer syncFlag;

    @Column("total_balance")
    private java.math.BigDecimal totalBalance;

    @Column("unstaked")
    private java.math.BigDecimal unstaked;

    @Column("updated_at")
    private java.util.Date updatedAt;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public java.util.Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(java.util.Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getCreatedBlockNum() {
        return createdBlockNum;
    }

    public void setCreatedBlockNum(Long createdBlockNum) {
        this.createdBlockNum = createdBlockNum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.math.BigDecimal getStaked() {
        return staked;
    }

    public void setStaked(java.math.BigDecimal staked) {
        this.staked = staked;
    }

    public Integer getSyncFlag() {
        return syncFlag;
    }

    public void setSyncFlag(Integer syncFlag) {
        this.syncFlag = syncFlag;
    }

    public java.math.BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(java.math.BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }

    public java.math.BigDecimal getUnstaked() {
        return unstaked;
    }

    public void setUnstaked(java.math.BigDecimal unstaked) {
        this.unstaked = unstaked;
    }

    public java.util.Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(java.util.Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}

