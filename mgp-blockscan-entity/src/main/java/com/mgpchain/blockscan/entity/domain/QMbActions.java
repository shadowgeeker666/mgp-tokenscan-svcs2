package com.mgpchain.blockscan.entity.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QMbActions is a Querydsl query type for MbActions
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QMbActions extends com.querydsl.sql.RelationalPathBase<MbActions> {

    private static final long serialVersionUID = 1000493551;

    public static final QMbActions mbActions = new QMbActions("mb_actions");

    public final StringPath account = createString("account");

    public final DateTimePath<java.util.Date> actAt = createDateTime("actAt", java.util.Date.class);

    public final NumberPath<Long> blockNum = createNumber("blockNum", Long.class);

    public final DateTimePath<java.util.Date> createdAt = createDateTime("createdAt", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final StringPath trace = createString("trace");

    public final StringPath transactionId = createString("transactionId");

    public final DateTimePath<java.util.Date> updatedAt = createDateTime("updatedAt", java.util.Date.class);

    public final com.querydsl.sql.PrimaryKey<MbActions> primary = createPrimaryKey(id);

    public QMbActions(String variable) {
        super(MbActions.class, forVariable(variable), "null", "mb_actions");
        addMetadata();
    }

    public QMbActions(String variable, String schema, String table) {
        super(MbActions.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QMbActions(String variable, String schema) {
        super(MbActions.class, forVariable(variable), schema, "mb_actions");
        addMetadata();
    }

    public QMbActions(Path<? extends MbActions> path) {
        super(path.getType(), path.getMetadata(), "null", "mb_actions");
        addMetadata();
    }

    public QMbActions(PathMetadata metadata) {
        super(MbActions.class, metadata, "null", "mb_actions");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(account, ColumnMetadata.named("account").withIndex(5).ofType(Types.VARCHAR).withSize(255));
        addMetadata(actAt, ColumnMetadata.named("act_at").withIndex(4).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(blockNum, ColumnMetadata.named("block_num").withIndex(2).ofType(Types.BIGINT).withSize(19));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(8).ofType(Types.TIMESTAMP).withSize(19));
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT).withSize(20).notNull());
        addMetadata(name, ColumnMetadata.named("name").withIndex(6).ofType(Types.VARCHAR).withSize(255));
        addMetadata(trace, ColumnMetadata.named("trace").withIndex(7).ofType(Types.LONGVARCHAR).withSize(2147483647));
        addMetadata(transactionId, ColumnMetadata.named("transaction_id").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(9).ofType(Types.TIMESTAMP).withSize(19));
    }

}

